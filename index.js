const express = require('express') //load express module
const request = require('request'); // http request
const {
    serverURL,
    key
    
} = require('./config/index');
const app = express();
 


                                  //------------building promise-------------------//

const building1 = async (token) =>{ 
    
    return new Promise(async (resolve, resject) => {
        const options1 = {
            url: serverURL+'/buildings',
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'X-API-EMAIL': 'msalmansohail16@gmail.com',
                'X-API-KEY': ` ${key}` 
            }
        };
        await request(options1, (err, response, body) => {
            if(err) {
                resolve ({code:response.statusCode, message:err})
            }
            else{
                let jsonBody1 = JSON.parse(body);
                resolve ({code:response.statusCode, message:jsonBody1})
            }
        });
    })
}

app.get('/building', async(req, res) => {
    var b = await building1(req.headers.authorization)
    res.status(200).json(b)

    
})
                                         //-----------------end promise --------------------//



                                                //---------flooor--------------
const floors = async (token1) =>{ 
    return new Promise(async (resolve, resject) => {
        const options2 = {
            url: serverURL+'/buildings/6114/floors',
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'X-API-EMAIL': 'msalmansohail16@gmail.com',
                'X-API-KEY': ` ${key}` 
            }
        };
        await request(options2, (err, response, body) => {
            if(err) {
                resolve ({code:response.statusCode, message:err})
            }
            else{
                let jsonBody2 = JSON.parse(body);
                resolve ({code:response.statusCode, message:jsonBody2})
            }
        });
    })
}

app.get('/floor', async(req, res) => {
    var c = await floors(req.headers.authorization)
    res.status(200).json(c)

    
})
                             //-----------------end promise --------------------//


                                    //---------flooor by id --------------
const floorsbyid = async (token2) =>{ 
    return new Promise(async (resolve, resject) => {
        const options3 = {
            url: serverURL+'/floors/12253',
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'X-API-EMAIL': 'msalmansohail16@gmail.com',
                'X-API-KEY': ` ${key}` 
            }
        };
        await request(options3, (err, response, body) => {
            if(err) {
                resolve ({code:response.statusCode, message:err})
            }
            else{
                let jsonBody3 = JSON.parse(body);
                resolve ({code:response.statusCode, message:jsonBody3})
            }
        });
    })
}

app.get('/floor/1', async(req, res) => {
    var d = await floorsbyid(req.headers.authorization)
    res.status(200).json(d)

    
})
//-----------------end promise --------------------//

                                  //---------POI --------------
                                  const pios = async (token3) =>{ 
                                    return new Promise(async (resolve, resject) => {
                                        const options4 = {
                                            url: serverURL+'/buildings/6114/pois',
                                            method: 'GET',
                                            headers: {
                                                'Content-Type': 'application/json',
                                                'X-API-EMAIL': 'msalmansohail16@gmail.com',
                                                'X-API-KEY': ` ${key}` 
                                            }
                                        };
                                        await request(options4, (err, response, body) => {
                                            if(err) {
                                                resolve ({code:response.statusCode, message:err})
                                            }
                                            else{
                                                let jsonBody4 = JSON.parse(body);
                                                resolve ({code:response.statusCode, message:jsonBody4})
                                            }
                                        });
                                    })
                                }
                                
                                app.get('/poi', async(req, res) => {
                                    var e = await pios(req.headers.authorization)
                                    res.status(200).json(e)
                                })

                                //---------POsiton data in building  --------------
const positon = async (token4) =>{ 
    return new Promise(async (resolve, resject) => {
        const options6 = {
            url: serverURL+'/analytics/raw_pose_data',
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'X-API-EMAIL': 'msalmansohail16@gmail.com',
                'X-API-KEY': ` ${key}` 
            }
        };
        await request(options6, (err, response, body) => {
            if(err) {
                resolve ({code:response.statusCode, message:err})
            }
            else{
                let jsonBody6 = JSON.parse(body);
                resolve ({code:response.statusCode, message:jsonBody6})
            }
        });
    })
}

app.get('/position', async(req, res) => {
    var g = await positon(req.headers.authorization)
    res.status(200).json(g)
})


   
var port = process.env.port || 3000;

app.listen(port, console.log(`Server is listing to ${port}`));
